import os


class Writer:

    def write_matches_links(self, match_links):
        self.__write_to_disk(match_links, "/matches.csv")

    def write_to_disk(self, string_to_write, file_to_write):
        file = open(self.get_local_file(file_to_write), "w")
        file.write(string_to_write)

    def get_local_file(self, local_file):
        current_location = os.path.dirname(__file__)
        return os.path.join(current_location, '../', local_file)
