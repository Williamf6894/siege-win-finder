"""
This will read the list of matches from the Siege.gg website.
"""
import os

from bs4 import BeautifulSoup

from src.logger import Logger


class ParseResults:
    def __init__(self):
        self.logger = Logger(verbose=True)
        self.current_location = os.path.dirname(__file__)

    def get_all_local_files(self):
        pass

    def get_local_file(self, local_path):
        current_location = os.path.dirname(__file__)
        return os.path.join(current_location, '../', local_path)

    """
    There are a many pages of results, this gets the last page
    """
    def get_last_page_number(self, page):
        with open(self.get_local_file(page)) as landing_page:
            soup = BeautifulSoup(landing_page, 'html.parser')

        page_items = soup.find_all('a', {'class': 'page-link'})
        end_page_index = page_items[len(page_items) - 2].string
        return int(end_page_index)

    def parse_matches_list(self, file_to_scan, index_to_parse=None):

        all_matches = self.get_matches_from_file(file_to_scan)

        results = ""
        if index_to_parse is None:
            for match in all_matches:
                results += self.parse_match(match)
        else:
            results = self.parse_match(all_matches[index_to_parse])

        return results

    def get_matches_from_file(self, file_to_scan):
        file = self.get_local_file(file_to_scan)
        with open(file) as html_file:
            soup = BeautifulSoup(html_file, 'html.parser')

        # Extend over many pages
        all_results = soup.find('div', {'class': 'matches__results'})
        all_matches = all_results.findAll('a', {'class': 'match'})
        return all_matches

    def parse_match(self, match):
        season = match.find('span', {'class': 'match__season'}).text.strip()
        region = match.find('span', {'class': 'match__region'}).text.strip()
        match_teams = match.findAll('span', {'class': 'match__name'})
        team1_name = match_teams[0].text.strip()
        team2_name = match_teams[1].text.strip()

        match_scores = match.findAll('div', {'class': 'match__score'})
        team1_score = match_scores[0].text.strip()
        team2_score = match_scores[1].text.strip()

        result = season + ',' + region + ',' + team1_name + ',' + \
            team2_name + ',' + team1_score + ',' + team2_score

        if team1_score > team2_score:
            result += ',' + team1_name
        elif team2_score > team1_score:
            result += ',' + team2_name
        else:
            result += ',draw'

        return result

    def parse_match_link(self, match):
        match_link = match.get("href")
        return match_link
