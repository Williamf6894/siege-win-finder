from itertools import cycle

import requests
from bs4 import BeautifulSoup

from logger import Logger


class Proxy:

    def __init__(self):
        self.proxy_url = 'https://free-proxy-list.net/'
        self.httpbin_url = 'https://httpbin.org/ip'
        self.logger = Logger(True)

        # Could make this a parameter
        self.number_of_proxies = 10

    def get_proxies(self):
        response = requests.get(self.proxy_url)
        parser = BeautifulSoup(response.text, "html.parser")
        self.logger.info("Requesting new Proxy List")

        proxies = set()
        table_rows = parser.tbody.find_all("tr", limit=self.number_of_proxies)

        for i in table_rows:
            https = i.find("td", {"class": "hx"})
            table = i.find_all("td")
            ip = table[0]
            port = table[1]
            if https.text == "yes":
                # # Grabbing IP and corresponding PORT
                proxy = ":".join([ip.text, port.text])
                proxies.add(proxy)

        return proxies

    def filter_out_bad_proxies(self, proxies):
        working_proxies = set()
        proxy_pool = cycle(proxies)

        for i in range(1, len(proxies) + 1):
            a_proxy = next(proxy_pool)
            try:
                requests.get(self.httpbin_url, proxies={"http": a_proxy, "https": a_proxy})
                self.logger.info("Keeping " + a_proxy)
                working_proxies.add(a_proxy)
            except:
                # Most free proxies will often get connection errors.
                print("Skipping. Connection error")

        return working_proxies
