"""
Logger will print to the console and will only print with the verbose flag
For now
"""

import logging
from enum import Enum


class ErrorCode(Enum):
    INFO = 1
    WARN = 2
    ERROR = 3


class Logger:
    error_code = ErrorCode.ERROR
    message = "Unset error message"
    verbose = False

    def __init__(self, verbose, log_file=None):
        """
        Logger constructor
        :param verbose: Boolean for info & warning messages
        :param log_file: Name of the file for the logs.
        """
        self.logger = logging.getLogger()
        self.__set_verbosity(verbose)
        self.__create_stream_logger()
        self.add_file_logger(log_file)

    def __set_verbosity(self, verbose):
        if verbose:
            self.logger.setLevel(level=logging.DEBUG)
        else:
            self.logger.setLevel(level=logging.WARNING)

    def __create_stream_logger(self):
        """
        Sets the default configuration for the logging output to the console
        """
        stream_handler = logging.StreamHandler()
        stream_format = logging.Formatter('%(name)s: %(levelname)s - %(message)s')
        stream_handler.setFormatter(stream_format)
        self.logger.addHandler(stream_handler)

    def add_file_logger(self, log_file=None):
        if log_file is None:
            log_file = "logs.txt"
        self.__create_file_logger(log_file)

    def __create_file_logger(self, file):
        """
        Sets the default configuration for the logging output to the logs.txt file
        """
        file_handler = logging.FileHandler(file)
        file_format = logging.Formatter('%(asctime)s -> %(name)s: %(levelname)s - %(message)s')
        file_handler.setFormatter(file_format)
        self.logger.addHandler(file_handler)

    def log(self, error_code, message):
        self.error_code = error_code
        self.__print_error(message)

    def __print_error(self, message):
        """
        Depending on the ErrorCode given, the message will be logged a
        with different level of severity
        :param message: Message to log or the console and/or log files
        """
        if self.error_code == ErrorCode.INFO:
            self.logger.info(message)
        elif self.error_code == ErrorCode.WARN:
            self.logger.warning(message)
        elif self.error_code == ErrorCode.ERROR:
            self.logger.error(message)
        else:
            self.logger.debug(message)

    def info(self, message):
        self.log(ErrorCode.INFO, message)

    def warn(self, message):
        self.log(ErrorCode.WARN, message)

    def error(self, message):
        self.log(ErrorCode.ERROR, message)
