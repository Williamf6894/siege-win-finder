import random

import requests
from fake_useragent import UserAgent, FakeUserAgentError

from src.parse_results import ParseResults


class DownloadHTML:

    def __init__(self):
        self.state = True

    def set_new_header(self):
        """
        Get a new header sometimes to not upset the server
        """
        try:
            useragent = UserAgent(cache=False)
        except FakeUserAgentError:
            pass

        return useragent.random

    def get_random_delay(self):
        """
        Get a random time delay between 5-15 seconds to emulate a user reading
        :return: random int
        """
        return random.randint(5, 15)

    def download_match_list_pages(self):
        """
        Downloads the page
        :return: String of the page
        """
        typical_link = "https://siege.gg/matches?tab=results"
        start_page = requests.get(typical_link)

        parse = ParseResults(False)
        end_link_page_number = parse.get_last_page_number(start_page)
        # Get the first page
        # Check for the last page
        # TODO: Ensure correct page
        for i in range(2, int(end_link_page_number)):
            pass


    def get_single_match_list_page(self, page_number):
        """
        Get the next page link, identifiable from the page_number
        :return: Page with List of Matches
        """
        pass

    def store_list_pages(self, name, content):
        """
        Store the page in the pages folder and the information in a CSV file
        Its very likely that I will only need to get this information once, so I might be
        better to just store the info needed, then check if something is
        missing.
        :param name:
        :param content:
        :return:
        """
        pass

    def get_new_primary_key(self):
        """
        Give the page or match a primary key, either generate or get from
        the page itself.
        :return:
        """
        pass

    def compare_new_id_to_saved(self):
        """
        If we already have all the pages and this only matches one we have then
        we can stop the update, we don't need to get them rest all over again
        :return:
        """
        pass
