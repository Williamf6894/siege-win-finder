import os

from src.parse_results import ParseResults

parse = ParseResults()
example_list_page = 'example_pages/Siege_list.html'


class TestParseResults:

    def test_parse_results_get_example_page(self):
        not_file = ""
        siege_file = parse.get_local_file(example_list_page)
        assert os.path.isfile(siege_file)
        assert not os.path.isfile(not_file)

    def test_parse_example_page(self):
        example_file = parse.get_local_file(example_list_page)
        expected = "USN,NA,Obey Alliance,Akatsuki,7,3,Obey Alliance"
        unexpected = "USA,NA,Obey Alliance,Akatsuki,7,3,Obey Alliance"
        assert parse.parse_matches_list(example_file, 1) == expected
        assert parse.parse_matches_list(example_file, 1) != unexpected

    def test_get_last_page_number(self):
        expected = 50
        actual = parse.get_last_page_number(parse.get_local_file(example_list_page))
        print(actual)
        assert actual == expected

    def test_parse_match_link(self):
        link_expected = "/matches/2205"
        matches = parse.get_matches_from_file(example_list_page)
        result = parse.parse_match_link(matches[0])
        assert link_expected == result
