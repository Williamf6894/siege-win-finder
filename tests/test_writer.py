from src.writer import Writer

class TestWriter():

    def test_write_to_disk(self):
        self.writer = Writer()
        test_file = 'example_pages/test.csv'
        self.writer.write_to_disk('test', test_file)
        file = open(self.writer.get_local_file(test_file))
        assert file.read() == "test"
