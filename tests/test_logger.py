
from src.logger import Logger, ErrorCode

test_file = "test_logger.txt"

logger = Logger(True, test_file)


class TestLogger:

    def test_logger_save_file_info_and_debug(self):

        info = "Info to log"
        debug = "Debug to log"
        logger.log(ErrorCode.INFO, info)
        logger.log('', debug)

        actual = self.read_test_log_file()

        print(actual)
        assert actual.find('INFO - ' + info) != -1
        assert actual.find('DEBUG - ' + debug) != -1

    def test_logger_save_file_warning(self):
        warning = "Warning logged"
        logger.log(ErrorCode.WARN, warning)

        actual = self.read_test_log_file()
        assert actual.find('WARNING - ' + warning) != -1

    def test_logger_save_file_not_logged_text(self):
        not_logged = "Not logged"
        actual = self.read_test_log_file()
        assert actual.find(not_logged) == -1

    def read_test_log_file(self):
        with open(test_file, 'r') as f:
            return f.read()
